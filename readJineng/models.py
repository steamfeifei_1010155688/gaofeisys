from django.db import models
from django.forms import fields
from django.forms import  widgets
from django import forms
import re

# Create your models here.
# 技能
class Exam_jineng(models.Model):
    title = models.CharField(max_length=200)    #技能主題
    title_content = models.TextField()  #技能內容
    daima = models.TextField()  #技能代码

class Exam_jinengForm(forms.Form):
    exam_titles = Exam_jineng.objects.values_list('id', 'title')
    exam_titles1 = [i for i in exam_titles if not re.search(r'月考|周考',i[1])]
    exam_titles2 = [i for i in exam_titles if re.search(r'月考|周考',i[1])]
    # exam_titles1 = sorted(exam_titles1,key=lambda x:int(re.search(r'[-_]\w*(\d*)\w*[.|_]*',x[1]).group(1)),reverse=False)
    exam_titles1 = sorted(exam_titles1,key=lambda x:int(re.search(r'[-_][\u4e00-\u9fa5]*(\d+)\w+',x[1]).group(1)),reverse=False)
    exam_titles = exam_titles1 + exam_titles2

    print(Exam_jineng.objects.values('id','title'))
    title = fields.ChoiceField(
        choices=exam_titles,
        widget=widgets.Select(attrs={'class': 'form-control', 'style': 'width:60%;display:inline-block;','id':'id_jineng'}),
    )
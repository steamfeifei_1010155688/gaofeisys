from django.db import models

from purchaseSys.models import Cangku
from userInfo.models import User

from django.forms import fields
from django import forms
from django.forms import widgets

import time

# Create your models here.
#订单
class Discount(models.Model):
    dname = models.CharField(max_length=20)
class DiscountForm(forms.Form):
    dname = fields.ChoiceField(
        label='折扣',
        choices=Discount.objects.values_list('id','dname'),
        widget=widgets.Select(),
    )

class Order(models.Model):
    # id =  models.AutoField(primary_key=True)    #序号
    ono = models.FloatField()                   #订单编号
    ocangku = models.ForeignKey(Cangku,related_name='order',on_delete=models.CASCADE)       #对应仓库货物
    odiscount = models.ForeignKey(Discount,on_delete=models.CASCADE)  #折扣价
    ocount = models.IntegerField()              #销售数量
    oselldate = models.DateTimeField(auto_now_add=True)     #售卖日期
    ouser = models.OneToOneField(User,on_delete=models.CASCADE)  #售货员

class OrderForm(forms.Form):
    id = fields.IntegerField(
        label='订单序号',
        widget=widgets.HiddenInput(),
    )
    ono = fields.ChoiceField(
        label='订单编号',
        initial= time.time(),
        choices= Discount.objects.values_list('id','dname'),
        widget = widgets.Select(attrs={'class': 'form-control'}),
        required=True,
    )
    o_gno = fields.CharField(
        label='货号',
    )
    o_gname = fields.CharField(
        label='货物名称',
    )
    o_gsize = fields.CharField(
        label='货物尺寸',
    )
    o_gcolor = fields.CharField(
        label='货物颜色',
    )
    o_gtype = fields.CharField(
        label='货物种类',
    )
    o_gmer = fields.CharField(
        label='进货厂商',
    )
    ocount = fields.IntegerField(
        label='售卖数量',
    )



from django.shortcuts import render,HttpResponse

from sellSys.models import Order,Discount,OrderForm
from purchaseSys.models import Cangku,CangkuForm

import time

import json

# Create your views here.
def sellIndex(request):

    return render(request,'sellSys/sellIndex.html',locals())
    # return HttpResponse('success')


def sellorder(request):
    id = request.GET.get('id')
    one_order_temp = Order(
        ono = time.time(),
        ocangku = Cangku.objects.filter(id=id)[0],
        ocount = 1
    )
    goods_form = CangkuForm()
    order_form = OrderForm()
    return render(request,'sellSys/orderHanding_manage.html',locals())
'''
    @auth:gaofei
    文件名：pager_util.py
    功能：实现分页功能
'''

class Pagination(object):

    '''
        实现分页功能
        实例化参数：数据总数，当前页码，每页的数据数，每页显示的最大页数
    '''
    def __init__(self,totalCount,currentPage,perPageDatas = 15,perShowMaxPages = 7):
        self.totalCount = totalCount
        try:
            self.currentPage = int(currentPage)
            if self.currentPage < 1:
                raise Exception('当前页码传入错误')
        except:
            self.currentPage = 1

        self.perPageDatas = perPageDatas
        self.perShowMaxPages = perShowMaxPages

    # 当前页数据开始下标
    @property
    def begin(self):
        return (self.currentPage - 1 ) * self.perPageDatas

    # 当前页数据结束下标
    @property
    def end(self):
        return self.currentPage * self.perPageDatas

    #总页数
    @property
    def pageCount(self):
        a,b = divmod(self.totalCount,self.perPageDatas)
        return a if b == 0 else a + 1

    #获取当前页显示页码按钮范围
    @property
    def getPagesRange(self):
        pageCount = self.pageCount
        middle = self.perShowMaxPages // 2  # 一半的按钮数
        if self.currentPage <= middle + 1:  # 0  --- >   midddle
            return range(1, self.perShowMaxPages + 1)
        if self.currentPage >= pageCount - middle:
            return range(pageCount - self.perShowMaxPages + 1, pageCount + 1)
        return range(self.currentPage - middle, self.currentPage + middle + 1)

    # 获取分页网页按钮，无样式
    @property
    def getPageStr(self):
        curPageStrList = []

        firstPage = '<a href="/pagetest/?pn=1">首页</a>'
        curPageStrList.append(firstPage)

        if self.currentPage > 1:
            prePage = '<a href="/pagetest/?pn=%s">上一页</a>'%(self.currentPage - 1)
        else :
            prePage = '<a href="javascript:void(0);">上一页</a>'
        curPageStrList.append(prePage)

        for i in self.getPagesRange:
            temp = '<a href="/pagetest/?pn=%s">%s</a>'%(i,i)
            curPageStrList.append(temp)

        if self.pageCount <= self.currentPage:
            nextPage = '<a href="javascript:void(0);">下一页</a>'
        else:
            nextPage = '<a href="/pagetest/?pn=%s">下一页</a>'%(self.currentPage + 1)
        curPageStrList.append(nextPage)

        lastPage = '<a href="/pagetest/?pn=' + str(self.pageCount) + '">末尾</a>'
        curPageStrList.append(lastPage)

        return ''.join(curPageStrList)

    #使用插件管理分页按钮
    @property
    def getPageStr_BootStrap(self):
        '''
            使用BootStrap插件效果：
            1、下载插件：https://v3.bootcss.com/
            2、导入静态文件
            3、html导入静态文件下css样式
                形如：<link rel="stylesheet" href="/statics/plugins/bootstrap/css/bootstrap.min.css">

        '''

        curPageStrList = []

        curPageStrList.append('<nav aria-label="Page navigation"><ul class="pagination">')

        firstPage = '<li><a href="/pagetest/?pn=1">首页</a></li>'
        curPageStrList.append(firstPage)

        if self.currentPage > 1:
            prePage = '<li><a href="/pagetest/?pn=%s">上一页</a></li>'%(self.currentPage - 1)
        else :
            prePage = '<li><a href="javascript:void(0);">上一页</a></li>'
        curPageStrList.append(prePage)

        for i in self.getPagesRange:
            if i == self.currentPage:
                temp = '<li class ="active"><a href="/pagetest/?pn=%s">%s</a></li>' % (i, i)
            else:
                temp = '<li><a href="/pagetest/?pn=%s">%s</a></li>'%(i,i)
            curPageStrList.append(temp)

        if self.pageCount <= self.currentPage:
            nextPage = '<li><a href="javascript:void(0);">下一页</a></li>'
        else:
            nextPage = '<li><a href="/pagetest/?pn=%s">下一页</a></li>'%(self.currentPage + 1)
        curPageStrList.append(nextPage)

        lastPage = '<li><a href="/pagetest/?pn=' + str(self.pageCount) + '">末尾</a></li>'
        curPageStrList.append(lastPage)

        curPageStrList.append('</ul></nav>')
        return ''.join(curPageStrList)

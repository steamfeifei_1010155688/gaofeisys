"""gaofeisys URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include

import  sellSys.views as sv
import  purchaseSys.views as pv
import readJineng.views as rv1

#题库
import  readxls.views as rv

import lilun.views as lv

urlpatterns = [
    path('admin/', admin.site.urls),
    path('userinfo/', include('userInfo.urls')),
    path('purSysIndex/', include('purchaseSys.urls')),

    path('sellSysIndex/',include('sellSys.urls')),

    # path('',sv.sellIndex),
    # path('',pv.cangkuquery)

    path('xlsshow/',rv.show),
    path('', rv1.showjineng),
    path('showjineng1/',rv1.showjineng1),
    path('showlilun/',lv.showLilun),




]

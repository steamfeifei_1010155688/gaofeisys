from django.db import models
from django import forms
from django.forms import fields
from django.forms import widgets

from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator

import django.utils.timezone as timezone

# Create your models here.
#货物种类
class GoodsType(models.Model):
    gname = models.CharField(max_length=20)
class GoodsTypeForm(forms.Form):
    gname = fields.CharField(max_length=20,min_length=1,required=True,label='添加货物类型',error_messages={'required':'货物类型不能为空','max_length':'不能超过20个字符','min_length':'不能少于1个字符'}
                             ,widget=widgets.TextInput(attrs={'class':'form-control'}))

    #整体验证
    def clean_gname(self):
        value_dict = self.cleaned_data
        gname_ = value_dict.get('gname').strip()
        if GoodsType.objects.filter(gname=gname_):
            raise ValidationError('该货物类型已经存在')
        else:
            return self.cleaned_data['gname']

#进货厂商
class Merchant(models.Model):
    mname = models.CharField(max_length=20)

class MerchantForm(forms.Form):
    mname = fields.CharField(max_length=20,min_length=1,required=True,label='添加进货厂商',error_messages={'required':'进货厂商不能为空','max_length':'不能超过20个字符','min_length':'不能少于1个字符'}
                             ,widget=widgets.TextInput(attrs={'class':'form-control'}))

    #整体验证
    def clean_mname(self):
        value_dict = self.cleaned_data
        mname_ = value_dict.get('mname').strip()
        if Merchant.objects.filter(mname=mname_):
            raise ValidationError('该进货厂商已经存在')
        else:
            return self.cleaned_data['mname']

#尺寸
class Size(models.Model):
    sname = models.CharField(max_length=20)

class SizeForm(forms.Form):
    sname = fields.CharField(max_length=20,min_length=1,required=True,label='添加尺寸',error_messages={'required':'尺寸不能为空','max_length':'不能超过20个字符','min_length':'不能少于1个字符'}
                             ,widget=widgets.TextInput(attrs={'class':'form-control'}))

    #整体验证
    def clean_sname(self):
        value_dict = self.cleaned_data
        sname_ = value_dict.get('sname').strip()
        if Size.objects.filter(sname=sname_):
            raise ValidationError('该尺寸已经存在')
        else:
            return self.cleaned_data['sname']


#颜色
class Color(models.Model):
    cname = models.CharField(max_length=20)

class ColorForm(forms.Form):
    cname = fields.CharField(max_length=20,min_length=1,required=True,label='添加颜色',error_messages={'required':'颜色不能为空','max_length':'不能超过20个字符','min_length':'不能少于1个字符'}
                             ,widget=widgets.TextInput(attrs={'class':'form-control'}))

    #整体验证
    def clean_cname(self):
        value_dict = self.cleaned_data
        cname_ = value_dict.get('cname').strip()
        if Color.objects.filter(cname=cname_):
            raise ValidationError('该颜色已经存在')
        else:
            return self.cleaned_data['cname']

#仓库
class Cangku(models.Model):
    gno = models.CharField(max_length=20)   #货号
    gtype = models.ForeignKey('GoodsType',on_delete=models.CASCADE) #货物类型
    gname = models.CharField(max_length=20) #货物名称
    gmer = models.ForeignKey('Merchant',on_delete=models.CASCADE) #进货厂商
    gsize = models.ForeignKey('Size',on_delete=models.CASCADE)  #货物尺寸
    gcolor = models.ForeignKey('Color',on_delete=models.CASCADE)    #货物颜色
    gnums = models.IntegerField()   #进货总数量
    gcount = models.IntegerField()  #货物剩余数量
    ghandle = models.IntegerField() #货物挂起处理数量
    gprice1 = models.FloatField()   #进货价格
    gprice2 = models.FloatField()   #售卖价格
    gprofit = models.FloatField(default=0)  #利润/件
    gtime = models.DateTimeField() #进货时间

class CangkuForm(forms.Form):
    gno = fields.CharField(
        label='货号',
        max_length=20,
        min_length=1,
        required=True,
        error_messages={'required':'货号不能为空','max_length':'不能超过20个字符','min_length':'不能少于1个字符'},
        widget=widgets.TextInput(attrs={'class':'form-control cangku_upd'}),
        initial='00001'
    )
    gtype = fields.ChoiceField(
        label='货物类型',
        choices=GoodsType.objects.values_list('id','gname'),
        widget=widgets.Select(attrs={'class': 'form-control cangku_upd_01'}),
        initial=1,
    )
    gname = fields.CharField(
        label='货物名称',
        max_length=20,
        min_length=1,
        required=True,
        error_messages={'required':'货物名称不能为空','max_length':'不能超过20个字符','min_length':'不能少于1个字符'},
        widget=widgets.TextInput(attrs={'class': 'form-control cangku_upd'}),
        initial = 'gaofeifei',  #110
    )
    gmer = fields.ChoiceField(
        label='进货厂商',
        choices=Merchant.objects.values_list('id','mname'),
        widget=widgets.Select(attrs={'class': 'form-control cangku_upd_01'}),
        initial = 1,
    )
    gsize = fields.ChoiceField(
        label='货物尺寸',
        required=True,
        choices=Size.objects.values_list('id','sname'),
        widget=widgets.Select(attrs={'class': 'form-control cangku_upd_01'}),
        initial = 1,
    )
    gcolor = fields.ChoiceField(
        label='颜色',
        required=True,
        choices=Color.objects.values_list('id','cname'),
        widget=widgets.Select(attrs={'class': 'form-control cangku_upd_01'}),
        initial = 1,
    )
    gnums = fields.IntegerField(
        label='进货总数量',
        min_value=0,
        required=True,
        error_messages={'required': '进货总数量不能为空','min_value':'进货总数量不能为负数'},
        widget=widgets.NumberInput(attrs={'class': 'form-control cangku_upd'}),
        initial = 1,
    )
    gcount = fields.IntegerField(
        label='剩余数量',
        min_value=0,
        required=True,
        error_messages={'required': '货物剩余数量不能为空','min_value':'货物剩余数量不能为负数'},
        widget=widgets.NumberInput(attrs={'class': 'form-control cangku_upd'}),
        initial=1,
    )
    ghandle = fields.IntegerField(
        label='挂起数量',
        min_value=0,
        required=True,
        error_messages={'required': '货物挂起处理数量不能为空','min_value':'货物挂起处理数量不能为负数'},
        widget=widgets.NumberInput(attrs={'class': 'form-control cangku_upd'}),
        initial=1,
    )
    gprice1 = fields.FloatField(
        label='进货价格',
        min_value=0,
        error_messages={'required': '进货价格不能为空','min_value':'进货价格不能为负数'},
        widget=widgets.NumberInput(attrs={'class': 'form-control cangku_upd'}),
        initial=1,
    )
    gprice2 = fields.FloatField(
        label='售卖价格',
        min_value=0,
        error_messages={'required': '售卖价格不能为空','min_value':'售卖价格不能为负数'},
        widget=widgets.NumberInput(attrs={'class': 'form-control cangku_upd'}),
        initial=1,
    )
    # gprofit = fields.FloatField(
    #     label='利润/件',
    #     min_value=0,
    #     error_messages={'required': '利润/件不能为空','min_value':'利润/件不能为负数'},
    #     widget=widgets.NumberInput(attrs={'class': 'form-control'}),
    #     initial=1,
    # )
    gtime = fields.DateField(
        label='进货时间',
        widget=widgets.DateTimeInput(attrs={'class': 'form-control cangku_upd','type':'date'})

    )

#仓库查询组件
class CangkuQueryForm(forms.Form):
    gno = fields.CharField(
        label='货号',
        max_length=20,
        min_length=1,
        error_messages={'required':'货号不能为空','max_length':'不能超过20个字符','min_length':'不能少于1个字符'},
        widget=widgets.TextInput(attrs={'class':'form-control cangku_upd'}),
        # initial='00001',
        required=False,
    )
    gtype = fields.ChoiceField(
        label='货物类型',
        choices= [('0','-------请选择--------')]+list(GoodsType.objects.values_list('id','gname')),
        widget=widgets.Select(attrs={'class': 'form-control cangku_upd_01'}),
        initial=0,
        required=False,
    )
    print(GoodsType.objects.values_list('id','gname'))
    gname = fields.CharField(
        label='货物名称',
        max_length=20,
        min_length=1,
        error_messages={'required':'货物名称不能为空','max_length':'不能超过20个字符','min_length':'不能少于1个字符'},
        widget=widgets.TextInput(attrs={'class': 'form-control cangku_upd'}),
        # initial = 'gaofeifei',  #110
        required=False,
    )
    gmer = fields.ChoiceField(
        label='进货厂商',
        choices=[('0','-------请选择--------')]+list(Merchant.objects.values_list('id','mname')),
        widget=widgets.Select(attrs={'class': 'form-control cangku_upd_01'}),
        initial = 0,
        required=False,
    )
    gsize = fields.ChoiceField(
        label='货物尺寸',
        choices=[('0','-------请选择--------')]+list(Size.objects.values_list('id','sname')),
        widget=widgets.Select(attrs={'class': 'form-control cangku_upd_01'}),
        initial = 0,
        required=False,
    )
    gcolor = fields.ChoiceField(
        label='颜色',
        choices=[('0','-------请选择--------')]+list(Color.objects.values_list('id','cname')),
        widget=widgets.Select(attrs={'class': 'form-control cangku_upd_01'}),
        initial = 0,
        required=False,
    )
    gnums = fields.IntegerField(
        label='进货总数量',
        min_value=0,
        error_messages={'required': '进货总数量不能为空','min_value':'进货总数量不能为负数'},
        widget=widgets.NumberInput(attrs={'class': 'form-control cangku_upd'}),
        # initial = 1,
        required=False,
    )
    gcount = fields.IntegerField(
        label='剩余数量',
        min_value=0,
        error_messages={'required': '货物剩余数量不能为空','min_value':'货物剩余数量不能为负数'},
        widget=widgets.NumberInput(attrs={'class': 'form-control cangku_upd'}),
        # initial=1,
        required=False,
    )
    ghandle = fields.IntegerField(
        label='挂起数量',
        min_value=0,
        error_messages={'required': '货物挂起处理数量不能为空','min_value':'货物挂起处理数量不能为负数'},
        widget=widgets.NumberInput(attrs={'class': 'form-control cangku_upd'}),
        # initial=1,
        required=False,
    )
    gprice1 = fields.FloatField(
        label='进货价格',
        min_value=0,
        error_messages={'required': '进货价格不能为空','min_value':'进货价格不能为负数'},
        widget=widgets.NumberInput(attrs={'class': 'form-control cangku_upd'}),
        # initial=1,
        required=False,
    )
    gprice2 = fields.FloatField(
        label='售卖价格',
        min_value=0,
        error_messages={'required': '售卖价格不能为空','min_value':'售卖价格不能为负数'},
        widget=widgets.NumberInput(attrs={'class': 'form-control cangku_upd'}),
        # initial=1,
        required=False,
    )
    # gprofit = fields.FloatField(
    #     label='利润/件',
    #     min_value=0,
    #     error_messages={'required': '利润/件不能为空','min_value':'利润/件不能为负数'},
    #     widget=widgets.NumberInput(attrs={'class': 'form-control'}),
    #     initial=1,
    # )
    gtime = fields.DateField(
        label='进货时间',
        widget=widgets.DateTimeInput(attrs={'class': 'form-control cangku_upd','type':'date'}),
        required=False,

    )

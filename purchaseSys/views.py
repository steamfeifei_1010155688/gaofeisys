from django.shortcuts import render,HttpResponse
from purchaseSys.models import GoodsType,GoodsTypeForm
from purchaseSys.models import Merchant,MerchantForm
from purchaseSys.models import Size,SizeForm
from purchaseSys.models import Color,ColorForm
from purchaseSys.models import Cangku,CangkuForm
from purchaseSys.models import CangkuQueryForm
import json
# Create your views here.
def purSysIndex(request):

    return render(request,'purchaseSys/purSysIndex.html')
def cangkuquery(request):
    status_str = {'status':200,'msg':'测试通过'}

    cangku_form = CangkuQueryForm()
    if request.method == 'GET':
        all_cangku = Cangku.objects.all()
    else:
        print(request.POST)
        post = request.POST
        # {'gno': [''], 'gtype': ['0'], 'gname': [''], 'gmer': ['0'], 'gsize': ['0'], 'gcolor': ['0'], 'gnums': [''],
        # 'gcount': [''], 'ghandle': [''], 'gprice1': [''], 'gprice2': [''], 'gtime': ['']}

        cangku_form = CangkuQueryForm(request.POST)
        if cangku_form.is_valid():

            if post.get('operate').strip() != 'que':
                if post.get('operate').strip() == 'del':
                    Cangku.objects.filter(id=post.get('id').strip()).delete()
                    msg_del = '删除成功'
                    status_str['msg'] = msg_del
                elif post.get('operate').strip() == 'upd':
                    cangku_form = CangkuForm(request.POST)

                    print(1111111111,'验证成功')    #110
                    cangku_data = cangku_form.cleaned_data
                    print(type(cangku_data),cangku_data)  #110

                    cangku_data['gtype'] = GoodsType.objects.get(id=cangku_data.get('gtype').strip())
                    cangku_data['gmer'] = Merchant.objects.get(id=cangku_data.get('gmer').strip())
                    cangku_data['gsize'] = Size.objects.get(id=cangku_data.get('gsize').strip())
                    cangku_data['gcolor'] = Color.objects.get(id=cangku_data.get('gcolor').strip())
                    cangku_data['gprofit'] = float(cangku_data.get('gprice2')) - float(cangku_data.get('gprice1'))

                    Cangku.objects.filter(id=post.get('id').strip()).update(**cangku_data)
                    msg_upd = '修改成功'
                    status_str['msg'] = msg_upd

                print(status_str)
                return HttpResponse(json.dumps(status_str))
        else:
            msg_error = '格式错误'
            status_str['status'] = 201
            status_str['msg'] = msg_error
            return HttpResponse(json.dumps(status_str))

        #回显
        result = {}
        for x, y in request.POST.items():
            # print(x,y)
            y = y.strip()
            if x in ['gtype', 'gmer', 'gsize', 'gcolor']:
                if y != '0':
                    result[x + '_id'] = y
            elif y != '':
                result[x] = y
        print(result)
        # select_cangku = Cangku.objects.filter(**result)
        if len(result) == 0:
            all_cangku = Cangku.objects.all()
        else:
            print(result)
            result.pop('operate')
            all_cangku = Cangku.objects.filter(**result)
        msg_que = '查询成功'

    return render(request,'purchaseSys/cangkuquery_manage.html',locals())

def cangku(request):
    cangku_form = CangkuForm()
    all_cangku = Cangku.objects.all()


    if request.method == 'POST':
        post = request.POST
        print(post) #110

        if post.get('operate').strip() == 'del':
            Cangku.objects.filter(id=post.get('id').strip()).delete()
            msg_del = '删除成功'
        else:
            cangku_form = CangkuForm(request.POST)

            if cangku_form.is_valid():
                print(1111111111,'验证成功')    #110
                cangku_data = cangku_form.cleaned_data
                print(type(cangku_data),cangku_data)  #110

                cangku_data['gtype'] = GoodsType.objects.get(id=cangku_data.get('gtype').strip())
                cangku_data['gmer'] = Merchant.objects.get(id=cangku_data.get('gmer').strip())
                cangku_data['gsize'] = Size.objects.get(id=cangku_data.get('gsize').strip())
                cangku_data['gcolor'] = Color.objects.get(id=cangku_data.get('gcolor').strip())
                cangku_data['gprofit'] = float(cangku_data.get('gprice2')) - float(cangku_data.get('gprice1'))

                if post.get('operate') == 'upd':
                    Cangku.objects.filter(id=post.get('id').strip()).update(**cangku_data)
                    msg_upd = '修改成功'
                elif post.get('operate') == 'add':
                    Cangku.objects.create(**cangku_data)
                    msg_add = '保存成功'
            else:
                print(222222222,'验证失败')
                print(cangku_form.errors)
                msg_add = '保存失败'


    return render(request,'purchaseSys/cangku_manage.html',locals())

def goodstype(request):
    # if request.method == 'GET':
        goods_type_form = GoodsTypeForm()
        all_goodstypes = GoodsType.objects.all()     #所有货物用于页面展示
        # return render(request,'purchaseSys/goodstype_manage.html',locals())
    # else:
        if request.method == "POST":
            post = request.POST
            print(post) #110
            goods_type_form = GoodsTypeForm(request.POST)

            if post.get('operate') == 'del':
                if GoodsType.objects.filter(id=post.get('id')).delete():
                    msg_del = '删除成功'
                else:
                    msg_del = '删除失败'
                goods_type_form = GoodsTypeForm()
            else:
                if goods_type_form.is_valid():
                    print('11111111111')    #110
                    goods_type_form_data = goods_type_form.cleaned_data
                    print('goods_type_form_data:',goods_type_form_data)

                    print(post.get('operate')) #110
                    if post.get('operate') == 'upd':
                        print(goods_type_form_data.get('gname').strip())
                        GoodsType.objects.filter(id=post.get('id').strip()).update(gname=goods_type_form_data.get('gname').strip())
                        msg_upd = '更新成功'
                        goods_type_form = GoodsTypeForm()

                    if post.get('operate') == 'add':
                        GoodsType.objects.create(gname=goods_type_form_data.get('gname'))
                        msg_add = '添加成功'
                        goods_type_form = GoodsTypeForm()


                else:
                    if post.get('operate') == 'upd':
                        msg_upd = '更新失败'
                        error_upd = goods_type_form.errors.get('gname')[0]
                        print('111111', error_upd)  # 110

                    if post.get('operate') == 'add':
                        msg_add = '添加失败'
                        error_add = goods_type_form.errors.get('gname')[0]
                        print('111111', error_add)  # 110


        return render(request, 'purchaseSys/goodstype_manage.html', locals())


def merchant(request):
    merchant_form = MerchantForm()
    all_merchant = Merchant.objects.all()  # 所有货物用于页面展示
    if request.method == "POST":
        post = request.POST
        print(post)  # 110
        merchant_form = MerchantForm(request.POST)

        if post.get('operate') == 'del':
            if Merchant.objects.filter(id=post.get('id')).delete():
                msg_del = '删除成功'
            else:
                msg_del = '删除失败'
                merchant_form = MerchantForm()
        else:
            if merchant_form.is_valid():
                print('11111111111')  # 110
                merchant_form_data = merchant_form.cleaned_data
                print('merchant_form_data:', merchant_form_data)

                print(post.get('operate'))  # 110
                if post.get('operate') == 'upd':
                    print(merchant_form_data.get('mname').strip())
                    Merchant.objects.filter(id=post.get('id').strip()).update(
                        mname=merchant_form_data.get('mname').strip())
                    msg_upd = '更新成功'
                    merchant_form = MerchantForm()

                if post.get('operate') == 'add':
                    Merchant.objects.create(mname=merchant_form_data.get('mname'))
                    msg_add = '添加成功'
                    merchant_form = MerchantForm()


            else:
                if post.get('operate') == 'upd':
                    msg_upd = '更新失败'
                    error_upd = merchant_form.errors.get('mname')[0]
                    print('111111', error_upd)  # 110

                if post.get('operate') == 'add':
                    msg_add = '添加失败'
                    error_add = merchant_form.errors.get('mname')[0]
                    print('111111', error_add)  # 110

    return render(request, 'purchaseSys/merchant_manage.html', locals())

def size(request):
    size_form = SizeForm()
    all_size = Size.objects.all()  # 所有货物用于页面展示
    if request.method == "POST":
        post = request.POST
        print(post)  # 110
        size_form = SizeForm(request.POST)

        if post.get('operate') == 'del':
            if Size.objects.filter(id=post.get('id')).delete():
                msg_del = '删除成功'
            else:
                msg_del = '删除失败'
                size_form = SizeForm()
        else:
            if size_form.is_valid():
                print('11111111111')  # 110
                size_form_data = size_form.cleaned_data
                print('size_form_data:', size_form_data)

                print(post.get('operate'))  # 110
                if post.get('operate') == 'upd':
                    print(size_form_data.get('sname').strip())
                    Size.objects.filter(id=post.get('id').strip()).update(
                        sname=size_form_data.get('sname').strip())
                    msg_upd = '更新成功'
                    size_form = SizeForm()

                if post.get('operate') == 'add':
                    Size.objects.create(sname=size_form_data.get('sname'))
                    msg_add = '添加成功'
                    size_form = SizeForm()


            else:
                if post.get('operate') == 'upd':
                    msg_upd = '更新失败'
                    error_upd = size_form.errors.get('sname')[0]
                    print('111111', error_upd)  # 110

                if post.get('operate') == 'add':
                    msg_add = '添加失败'
                    error_add = size_form.errors.get('sname')[0]
                    print('111111', error_add)  # 110

    return render(request, 'purchaseSys/size_manage.html', locals())

def color(request):
    color_form = ColorForm()
    all_color = Color.objects.all()  # 所有货物用于页面展示
    if request.method == "POST":
        post = request.POST
        print(post)  # 110
        color_form = ColorForm(request.POST)

        if post.get('operate') == 'del':
            if Color.objects.filter(id=post.get('id')).delete():
                msg_del = '删除成功'
            else:
                msg_del = '删除失败'
                color_form = ColorForm()
        else:
            if color_form.is_valid():
                print('11111111111')  # 110
                color_form_data = color_form.cleaned_data
                print('color_form_data:', color_form_data)

                print(post.get('operate'))  # 110
                if post.get('operate') == 'upd':
                    print(color_form_data.get('cname').strip())
                    Color.objects.filter(id=post.get('id').strip()).update(
                        cname=color_form_data.get('cname').strip())
                    msg_upd = '更新成功'
                    color_form = ColorForm()

                if post.get('operate') == 'add':
                    Color.objects.create(cname=color_form_data.get('cname'))
                    msg_add = '添加成功'
                    color_form = ColorForm()


            else:
                if post.get('operate') == 'upd':
                    msg_upd = '更新失败'
                    error_upd = color_form.errors.get('cname')[0]
                    print('111111', error_upd)  # 110

                if post.get('operate') == 'add':
                    msg_add = '添加失败'
                    error_add = color_form.errors.get('cname')[0]
                    print('111111', error_add)  # 110

    return render(request, 'purchaseSys/color_manage.html', locals())

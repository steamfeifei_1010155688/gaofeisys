from django.db import models

# Create your models here.
class User( models.Model):
    uname = models.CharField(max_length=20,unique=True)
    upwd = models.CharField(max_length=20)
    ulev = models.IntegerField(default=2)   #默认待审核
    utel = models.CharField(max_length=11)
    uaddr = models.CharField(max_length=32)
    uimg = models.CharField(max_length=100)


from django.apps import AppConfig


class ReadxlsConfig(AppConfig):
    name = 'readxls'

from django.db import models
from django.forms import fields
from django.forms import  widgets
from django import forms

# Create your models here.
#试卷
class Exam(models.Model):
    kaoti = models.ForeignKey('Kaoti',on_delete=models.CASCADE)

    bianhao = models.IntegerField()
    zhishidian = models.CharField(max_length=20)  #知识点
    leixing =   models.CharField(max_length=20)  #类型
    tigan =     models.CharField(max_length=200)  #题干
    xxa =       models.TextField()  #选项A
    xxb =       models.TextField()  #选项B
    xxc =       models.TextField()  #选项C
    xxd =       models.TextField()  #选项D
    xxe =       models.TextField()  #选项E
    xxf =       models.TextField()  #选项F
    daan =      models.CharField(max_length=100) #答案
    jiexi =     models.CharField(max_length=100)   #解析
    chutiren =  models.CharField(max_length=100) #出题人

#试卷名称
class Kaoti(models.Model):
    name = models.CharField(max_length=100)


import re
class KaotiForm(forms.Form):
    kaoti_titiles = tuple(Kaoti.objects.values_list('id', 'name'))
    kaoti_titiles1 = [i  for i in kaoti_titiles if not re.search(r'(月考|周考)',i[1])]
    kaoti_titiles2 = [i  for i in kaoti_titiles if re.search(r'(月考|周考)',i[1])]
    kaoti_titiles1 = sorted(kaoti_titiles1,key=lambda x:int(re.search(r'[-_](\d*)[.|_]*',x[1]).group(1)),reverse=False)
    kaoti_titiles1 += kaoti_titiles2

    kaoti_titiles3 = []
    for i in range(len(kaoti_titiles1)):
        kaoti_titiles3.append((kaoti_titiles1[i][0],str(i+1).rjust(2,'0')+'-' + kaoti_titiles1[i][1]))
    print(kaoti_titiles3)

    kaoti = fields.ChoiceField(
        choices=kaoti_titiles3,
        widget=widgets.Select(attrs={'class': 'form-control','style':'width:60%;display:inline-block;'}),
    )
# class KaotiForm(forms.Form):
#     name = fields.
from django.db import models
from django.forms import fields
from django.forms import  widgets
from django import forms

# Create your models here
class Lilun(models.Model):
    name = models.CharField(max_length=100)
    content = models.CharField(max_length=100)

class LilunForm(forms.Form):
    name = fields.ChoiceField(
        choices=Lilun.objects.values_list('id','name'),
        widget=widgets.Select(attrs={'class': 'form-control','style':'width:30%;display:inline-block;'}),
    )